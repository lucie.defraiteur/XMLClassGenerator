﻿// Copyright by Gonzague Defraiteur, 2016 Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
// Gonzague Defraiteur
// 18/11/2016 06:13:25 PM
// Description: A program to generate C# automatic parse scripts from XML
/*
	Copyright (c) 2016 Gonzague Defraiteur.
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do 
	so, subject to the following conditions:
	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLClassGenerator
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length < 3)
			{
				return;
			}
			else
			{
				string inputFile = args[0];
				string outputFile = args[1];
				string documentTypeName = args[2];
				ClassFromXMLGenerator gen = ClassFromXMLGenerator.Parse(inputFile, documentTypeName);
				System.IO.File.WriteAllText(outputFile, gen.stringDoc);
			}
		}
	}
	public enum FieldType
	{
		Node = 0,
		Attribute = 1
	}
	public class ClassFromXMLGenerator
	{
		public Dictionary<string, Dictionary<string, bool>[]> parsedTypes = new Dictionary<string, Dictionary<string, bool>[]>();

		public static ClassFromXMLGenerator Parse(XmlDocument document, string DocumentTypeName)
		{
			return (new ClassFromXMLGenerator(document, DocumentTypeName));
		}
		public static ClassFromXMLGenerator Parse(string fileName, string DocumentTypeName)
		{
			XmlDocument node = new XmlDocument();
			node.Load(fileName);
			return (Parse(node, DocumentTypeName));
		}
		public void ImporteNode(XmlNode node)
		{
			if (!parsedTypes.ContainsKey(node.Name))
			{
				parsedTypes.Add(node.Name, new Dictionary<string, bool>[2]);
				parsedTypes[node.Name][0] = new Dictionary<string, bool>();
				parsedTypes[node.Name][1] = new Dictionary<string, bool>();
			}
			string nodeName = node.Name;
			string nodeValue = node.Value;
			string textContent = node.InnerText;
			XmlAttributeCollection attributes = node.Attributes;
			if (attributes != null)
			{
				foreach (XmlAttribute attribute in attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (!parsedTypes[node.Name][1].ContainsKey(attribute.Name))
					{
						parsedTypes[node.Name][1].Add(attribute.Name, true);
					}
				}
			}

			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (!parsedTypes[node.Name][0].ContainsKey(subNode.Name))
					{
						parsedTypes[node.Name][0].Add(subNode.Name, true);
					}
					ImporteNode(subNode);
				}
			}
		}

		public void ImporteNode(XmlDocument node)
		{
			if (!parsedTypes.ContainsKey(node.Name))
			{
				parsedTypes.Add(node.Name, new Dictionary<string, bool>[2]);
				parsedTypes[node.Name][0] = new Dictionary<string, bool>();
				parsedTypes[node.Name][1] = new Dictionary<string, bool>();
			}
			string nodeName = node.Name;
			string nodeValue = node.Value;
			string textContent = node.InnerText;
			XmlAttributeCollection attributes = node.Attributes;
			if (attributes != null)
			{
				foreach (XmlAttribute attribute in attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (!parsedTypes[node.Name][1].ContainsKey(attribute.Name))
					{
						parsedTypes[node.Name][1].Add(attribute.Name, true);
					}
				}
			}

			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (!parsedTypes[node.Name][0].ContainsKey(subNode.Name))
					{
						parsedTypes[node.Name][0].Add(subNode.Name, true);
					}
					ImporteNode(subNode);
				}
			}
		}
		public StringBuilder Tabs(int lvl)
		{
			StringBuilder res = new StringBuilder("");
			for (int i = 0; i < lvl; i++)
			{
				res.Append("\t");
			}
			return (res);
		}
		public StringBuilder FirstLetterUp(string str)
		{
			StringBuilder res = new StringBuilder();
			for (int i = 0; i < str.Length; i++)
			{
				if (i == 0)
					res.Append((str[i] >= 'a' && str[i] <= 'z') ? (char)((int)str[i] - (int)'a' + (int)'A') : str[i]);
				else
					res.Append(str[i]);
			}
			return (res);
		}
		public static Dictionary<string, bool> builtinTypes = new Dictionary<string, bool>() { { "string", false }, { "float", false }, { "int", false }, { "Array", false }, { "bool", false }, { "uint", false } };
		public Dictionary<string, bool> AdditionalTypeNames = new Dictionary<string, bool>();
		// changer tout privé après parceque ça peut etre appelé qu'une fois depuis constructeur.
		public StringBuilder GenerateClass(string _nodeName, bool isDoc = false, string docName = "")
		{
			string nodeName = _nodeName;
			if (!parsedTypes.ContainsKey(nodeName))
				return (new StringBuilder(""));

			Dictionary<string, bool>[] sons = parsedTypes[nodeName];

			StringBuilder res = new StringBuilder();

			if (builtinTypes.ContainsKey(nodeName) || AdditionalTypeNames.ContainsKey(nodeName))
			{
				nodeName = "_" + nodeName;
			}
			StringBuilder nodeNameToUpper = FirstLetterUp(nodeName);
			res.Append("public class ");
			if (isDoc)
			{
				res.Append(docName + "Document");
			}
			else
			{
				res.Append(nodeNameToUpper + "Element");
			}
			res.Append("\n");
			res.Append("{\n");
			if (isDoc)
			{
				res.Append("public static " + docName + "Document ImportFromFile(string fileName)\n{\n");
				res.Append("XmlDocument doc = new XmlDocument();\n");
				res.Append("doc.Load(fileName);\n");
				res.Append("var res = new ColladaDocument(doc);\nreturn (res);\n");

				res.Append("}\n");
			}
			res.Append("public override string ToString()\n{\n");
			res.Append("return (this.Text);\n}\n");
			/// CONSTRUCTOR
			if (isDoc)
			{
				res.Append("public " + docName + "Document(XmlDocument node)\n");
			}
			else
			{
				res.Append("public " + nodeNameToUpper + "Element(XmlNode node)\n");
			}
			res.Append("{\n");
			res.Append("this.Text = node.InnerText;\n");

			/// ATTRIBUTES
			if (sons[1].Keys.Count > 0)
			{
				res.Append("if (node.Attributes != null)\n");
				res.Append("{\n");
				res.Append("foreach (XmlNode attribute in node.Attributes)\n");
				res.Append("{\n");
				res.Append("string attributeName = attribute.Name;\n");
				res.Append("string attributeValue = attribute.Value;\n");
				int k = 0;
				foreach (string _key in sons[1].Keys)
				{
					string key = _key;
					if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
					{
						key = "_" + key;
					}
					if (parsedTypes.ContainsKey(_key) && parsedTypes[_key][0].Count > 0)
					{
						key = "_" + key;
					}
					string keyToUpper = FirstLetterUp(key).ToString();
					if (k != 0)
					{
						res.Append("else ");
					}
					res.Append("if (attributeName == \"" + _key + "\")\n{\n");
					res.Append("this." + key + " = new " + keyToUpper + "Attr(attribute.Name, attribute.Value);\n");
					res.Append("}\n");
					k++;
				}
				res.Append("}\n"); // foreach
				res.Append("}\n"); // ATTRIBUTES NULL?
			}
			if (sons[0].Keys.Count > 0)
			{
				int k = 0;
				res.Append("if (node.ChildNodes != null)\n{\n");
				res.Append("foreach (XmlNode subNode in node.ChildNodes)\n{\n");

				foreach (string _key in sons[0].Keys)
				{
					string key = _key;
					if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
					{
						key = "_" + key;
					}

					if (key != "#text")
					{
						string keyToUpper = FirstLetterUp(key).ToString();
						string keyType = keyToUpper + "Element";
						if (k != 0)
						{
							res.Append("else ");
						}
						res.Append("if (subNode.Name == \"" + _key + "\")\n{\n");
						res.Append("if (this." + key + "ElementList == null)\n{\n");
						res.Append("this." + key + "ElementList = new List<" + keyType + ">();\n");
						res.Append("}\n");
						res.Append("this." + key + "ElementList.Add(new " + keyType + "(subNode));\n");
						res.Append("}\n");
						k++;
					}
				}
				res.Append("if (subNode.Name == \"#text\")\n{\n");
				res.Append("this.Text = subNode.Value;\n");
				res.Append("}\n"); // last if

				res.Append("}\n"); // foreach
				res.Append("}\n"); // ChildNodes NULL?
			}

			res.Append("}\n"); // CONSTRUCTOR;
							   // END CONSTRUCTOR.
							   // ATTRIBUTES FIELDS
			res.Append("public string Text = \"\";\n");
			foreach (string _key in sons[1].Keys)
			{
				string key = _key;
				if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
				{
					key = "_" + key;
				}
				if (parsedTypes.ContainsKey(_key) && parsedTypes[_key][0].Count > 0)
				{
					key = "_" + key;
				}
				res.Append("public " + FirstLetterUp(key) + "Attr " + key + " = null;\n");
			}
			// CHILDNODES FIELDS
			foreach (string _key in sons[0].Keys)
			{
				string key = _key;
				if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
				{
					key = "_" + key;
				}
				if (key != "#text")
				{
					string keyToUpper = FirstLetterUp(key).ToString();
					string keyType = keyToUpper + "Element";
					res.Append("public List<" + keyType + "> " + key + "ElementList = null;\n");
					res.Append("public " + keyType + " " + key + "\n{\n");
					res.Append("get\n{\n");
					res.Append("if (" + key + "ElementList == null || " + key + "ElementList.Count == 0)\n\treturn(null);\n");
					res.Append("return (" + key + "ElementList[0]);\n");
					res.Append("}\n");
					res.Append("}\n");
				}
				;
			}


			// SUB CLASSES GENERATION
			// ATTRIBUTES CLASSES

			if (!isDoc)
				res.Append("}\n"); // CLASS;
			foreach (string _key in sons[1].Keys)
			{
				string key = _key;
				if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
				{
					key = "_" + key;
				}
				if (parsedTypes.ContainsKey(_key) && parsedTypes[_key][0].Count > 0)
				{
					key = "_" + key;
				}
				if (!(builtAttributeTypes.ContainsKey(key)))
				{
					builtAttributeTypes[key] = true;
					res.Append(GenerateAttribute(_key));

				}
			}
			// CHILDNODES CLASSES
			foreach (string _key in sons[0].Keys)
			{
				string key = _key;
				if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
				{
					key = "_" + key;
				}
				if (key != "#text" && !(builtElementTypes.ContainsKey(key)))
				{
					builtElementTypes[key] = true;
					res.Append(GenerateClass(_key));

				}
			}
			if (isDoc)
			{
				res.Append("}\n");
			}
			return (res);
		}
		public Dictionary<string, bool> builtAttributeTypes = new Dictionary<string, bool>();
		public Dictionary<string, bool> builtElementTypes = new Dictionary<string, bool>();
		public StringBuilder GenerateAttribute(string _key)
		{
			StringBuilder res = new StringBuilder("");
			string key = _key;
			if (builtinTypes.ContainsKey(key) || AdditionalTypeNames.ContainsKey(key))
			{
				key = "_" + key;
			}
			if (parsedTypes.ContainsKey(_key) && parsedTypes[_key][0].Count > 0)
			{
				key = "_" + key;
			}
			res.Append("public class " + FirstLetterUp(key) + "Attr\n{\n");

			res.Append("public override string ToString()\n{\n");
			res.Append("return (this.Value);\n}\n");

			res.Append("private string _Name = \"\";\n");

			res.Append("public string Name\n{\n");
			res.Append("get\n{\n");
			res.Append("return (this._Name);\n");
			res.Append("}\n");
			res.Append("}\n");

			res.Append("private string _Value = \"\";\n");
			res.Append("public string Value\n{\n");
			res.Append("get\n{\n");
			res.Append("return (this._Value);\n");
			res.Append("}\n");
			res.Append("}\n");
			res.Append("public " + FirstLetterUp(key) + "Attr" + "(string name, string value)\n{\n");
			res.Append("this._Name = name;\n");
			res.Append("this._Value = value;\n");
			res.Append("}\n");

			res.Append("}\n");
			return (res);
		}
		public bool documentBuilt = false;
		public string stringDoc = "";
		public string AddTabs(StringBuilder str)
		{
			int lvl = 0;
			StringBuilder res = new StringBuilder();
			for (int i = 0; i < str.Length; i++)
			{
				if (str[i] == '}')
				{
					lvl--;
				}
				if (i > 0 && str[i - 1] == '\n')
				{
					res.Append(Tabs(lvl));
				}
				res.Append(str[i]);
				if (str[i] == '{')
				{
					lvl++;
				}
			}
			return (res.ToString());
		}

		public ClassFromXMLGenerator(XmlDocument node, string DocumentTypeName)
		{
			ImporteNode(node);
			this.DocumentTypeName = DocumentTypeName;

			StringBuilder copyright = new StringBuilder(@"// Copyright by Gonzague Defraiteur, 2016 Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
// Gonzague Defraiteur
// 18/11/2016 06:13:25 PM
// Description: A program to parse " + DocumentTypeName + " Documents and store them in C# classes." + @"
/*" + @"

	Copyright (c) 2016 Gonzague Defraiteur.
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the" + "\"Software\"" + @"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do 
	so, subject to the following conditions:
	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED" + " \"AS IS\"" + @", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */");
			StringBuilder DocumentClass = new StringBuilder(copyright + "\n\nusing System;\nusing System.Xml;\nusing System.Collections.Generic;\n\n");
			DocumentClass.Append(GenerateClass(node.Name, true, DocumentTypeName));
			this.stringDoc = AddTabs(DocumentClass);
		}
		public string DocumentTypeName = "";
	}
}
